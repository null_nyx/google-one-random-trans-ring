// ==UserScript==
// @name         Random Trans Ring
// @namespace    https://gitlab.com/null_nyx/google-one-random-trans-ring
// @version      1.0
// @description  Random colors of trans flag replace your Google One membership ring
// @author       @null_nyx
// @match        *://*.google.com/*
// @grant        none
// ==/UserScript==

(function() {
  // Get the SVG element
  const svg = document.querySelector("#gb > div > div.gb_Pd > div.gb_b.gb_Kd.gb_Wf.gb_D > div > a > div > svg");

  // Get the list of colors
  const colors = ["#5BCEFA", "#F5A9B8", "#FFFFFF"];

  // Iterate over the SVG's paths and set the fill color to a random color from the list
  for (const path of svg.querySelectorAll("path")) {
    path.setAttribute("fill", colors[Math.floor(Math.random() * colors.length)]);
  }
})();
