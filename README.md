# Google One Random Trans Ring
Make your Google One membership ring random colors of the trans flag

## [Install](https://gitlab.com/null_nyx/google-one-random-trans-ring/-/raw/main/trans-ring.user.js)

**Requires a userscript manager such as [Violentmonkey](https://violentmonkey.github.io/) or [Tampermonkey](https://www.tampermonkey.net/).**
